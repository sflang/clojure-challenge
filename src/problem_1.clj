(ns problem-1)
(def invoice (clojure.edn/read-string (slurp "invoice.edn")))

(defn taxes-contains-valid-iva
  [taxes]
  (if (->> taxes (some #(and (= 19 (:tax/rate %)) (= (:tax/category %) :iva))))
    1
    0))

(defn retentions-contains-valid-ret-fuente
  [retentions]
  (if (->> retentions (some #(and (= 1 (:retention/rate %)) (= (:retention/category %) :ret_fuente))))
    1
    0))

(defn logical-comparison
  [item]
  (bit-xor
   (->> item (:taxable/taxes) (taxes-contains-valid-iva))
   (->> item (:retentionable/retentions) (retentions-contains-valid-ret-fuente))))

(defn valid-items
  [invoice]
  (->> invoice (:invoice/items) (filter #(= 1 (logical-comparison %)))))

(valid-items invoice)
