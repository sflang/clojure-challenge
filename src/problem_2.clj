(ns problem-2 (:require [clojure.data.json :as json]
                        [clojure.spec.alpha :as s]))

(defn customer-info
  [invoice-json] {:customer/email (->> invoice-json (:customer) (:email))
                  :customer/name (->> invoice-json (:customer) (:company_name))})

(defn create-invoice-items
  [invoice-json]
  (->> invoice-json (:items) (map #(identity {:invoice-item/price (double (:price %))
                                              :invoice-item/quantity (double (:quantity %))
                                              :invoice-item/sku (:sku %)
                                              :invoice-item/taxes (->> % (:taxes) (map (fn [tax] {:tax/category :iva
                                                                                                  :tax/rate (double (:tax_rate tax))})) (vec))})) (vec)))
(defn create-invoice
  [filename]
  (let [invoice-json (->> filename (slurp) (clojure.data.json/read-str) (clojure.walk/keywordize-keys) (:invoice))]
    {:invoice/customer (customer-info invoice-json)
     :invoice/issue-date (->> invoice-json (:issue_date) (.parse (java.text.SimpleDateFormat. "dd/MM/yyyy")))
     :invoice/items (create-invoice-items invoice-json)}))

