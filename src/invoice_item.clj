(ns invoice-item)
(use 'clojure.test)

(defn- discount-factor [{:invoice-item/keys [discount-rate]
                         :or                {discount-rate 0}}]
  (- 1 (/ discount-rate 100.0)))

(defn subtotal
  [{:invoice-item/keys [precise-quantity precise-price discount-rate]
    :as                item
    :or                {discount-rate 0}}]
  (* precise-price precise-quantity (discount-factor item)))

(deftest works-fine-with-normal-input
  (testing "It works properly with normal input"
    (is (= 4.95 (subtotal {:invoice-item/precise-price 2.5
                           :invoice-item/precise-quantity 2
                           :invoice-item/discount-rate 1})))))

(deftest assumes-zero-discount-rate-if-not-present
  (testing "It assumes zero discount rate if it isn't present"
    (is (= 125 (subtotal {:invoice-item/precise-price 25
                          :invoice-item/precise-quantity 5})))))


(deftest return-zero-if-quantity-is-zero
  (testing "It returns zero if the quantity is 0"
    (is (= 0.0 (subtotal {:invoice-item/precise-price 2.5
                           :invoice-item/precise-quantity 0})))))

(deftest return-zero-if-quantity-is-zero-with-discount-rate
  (testing "It returns zero if the quantity is 0 even if discount rate is greater than zero"
    (is (= 0.0 (subtotal {:invoice-item/precise-price 2.5
                        :invoice-item/precise-quantity 0
                        :invoice-item/discount-rate 1})))))

(deftest works-fine-if-discount-rate-is-negative
  (testing "It should not increase the subtotal if discount rate is negative"
    (is (= 60.0 (subtotal {:invoice-item/precise-price 2
                          :invoice-item/precise-quantity 30
                          :invoice-item/discount-rate -1})))))

(deftest works-fine-if-price-is-negative
  (testing "It should return zero if price is negative"
    (is (= 0.0 (subtotal {:invoice-item/precise-price -2
                          :invoice-item/precise-quantity 30})))))

(deftest works-fine-if-quantity-is-negative
  (testing "It should return zero if quantity is negative"
    (is (= 0.0 (subtotal {:invoice-item/precise-price 20.0
                          :invoice-item/precise-quantity -4})))))
